resource "aws_security_group" "endpoints" {
  count       = var.ssm == "true" ? 1 : 0
  description = "Security group for VPC endpoints"
  name        = "${var.vpc-name}-endpoints"
  tags = {
    Name    = "${var.vpc-name}-endpoints"
  }
  vpc_id = aws_vpc.main.id
}

resource "aws_security_group_rule" "endpoints-https" {
  count       = var.ssm == "true" ? 1 : 0
  cidr_blocks = [
    aws_subnet.private-a.cidr_block,
    aws_subnet.private-b.cidr_block
  ]
  description       = "HTTPS access from private subnets"
  from_port         = 443
  protocol          = "tcp"
  security_group_id = aws_security_group.endpoints[0].id
  to_port           = 443
  type              = "ingress"
}
