resource "aws_route_table" "public" {
  tags = {
    Name = "${var.vpc-name}-public"
  }
  vpc_id = aws_vpc.main.id
}

resource "aws_route" "public" {
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.main.id
  route_table_id         = aws_route_table.public.id
}

resource "aws_route_table_association" "public-a" {
  route_table_id = aws_route_table.public.id
  subnet_id      = aws_subnet.public-a.id
}

resource "aws_route_table_association" "public-b" {
  route_table_id = aws_route_table.public.id
  subnet_id      = aws_subnet.public-b.id
}

resource "aws_route_table" "private-a" {
  tags = {
    Name = "${var.vpc-name}-private-a"
  }
  vpc_id = aws_vpc.main.id
}

resource "aws_route" "private-a" {
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id         = aws_nat_gateway.a.id
  route_table_id         = aws_route_table.private-a.id
}

resource "aws_route_table_association" "private-a" {
  route_table_id = aws_route_table.private-a.id
  subnet_id      = aws_subnet.private-a.id
}

resource "aws_route_table" "private-b" {
  tags  = {
    Name = "${var.vpc-name}-private-b"
  }
  vpc_id = aws_vpc.main.id
}

resource "aws_route" "private-b" {
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id         = aws_nat_gateway.b.id
  route_table_id         = aws_route_table.private-b.id
}

resource "aws_route_table_association" "private-b" {
  route_table_id = aws_route_table.private-b.id
  subnet_id      = aws_subnet.private-b.id
}
