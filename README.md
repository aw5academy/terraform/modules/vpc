# vpc
A terraform module to create a minimal VPC in the `us-east-1` region. Two public subnets and two private subnets will be created in the `us-east-1a` and ùs-east-1b` availability zones.

Optionally pass variable `ssm = true` to create VPC endpoints needed to use AWS Systems Manager - Session Manager.

# Usage
The module can be used with:
```
module "vpc" {
  source   = "git::https://gitlab.com/aw5academy/terraform/modules/vpc.git"
  vpc-name = "my-vpc"
}
```
