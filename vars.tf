variable "vpc-name" {
  description = "The name of the VPC."
  default     = "my-vpc"
}

variable "ssm" {
  description = "Whether or not to create VPC endpoints to allow Session Manager usage."
  default     = "false"
}
