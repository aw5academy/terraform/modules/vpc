resource "aws_internet_gateway" "main" {
  tags = {
    Name = var.vpc-name
  }
  vpc_id = aws_vpc.main.id
}

resource "aws_eip" "nat-a" {
  tags = {
    Name = "${var.vpc-name}-a"
  }
  vpc = true
}

resource "aws_nat_gateway" "a" {
  allocation_id = aws_eip.nat-a.id
  subnet_id     = aws_subnet.public-a.id
  tags = {
    Name = "${var.vpc-name}-a"
  }
}

resource "aws_eip" "nat-b" {
  tags  = {
    Name = "${var.vpc-name}-b"
  }
  vpc = true
}

resource "aws_nat_gateway" "b" {
  allocation_id = aws_eip.nat-b.id
  subnet_id     = aws_subnet.public-b.id
  tags = {
    Name = "${var.vpc-name}-b"
  }
}
