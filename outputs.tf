output "nat-a-public-ip" {
  value = aws_eip.nat-a.public_ip
}

output "nat-b-public-ip" {
  value = aws_eip.nat-b.public_ip
}

output "private-a-subnet-cidr" {
  value = aws_subnet.private-a.cidr_block
}

output "private-b-subnet-cidr" {
  value = aws_subnet.private-b.cidr_block
}

output "private-a-subnet-id" {
  value = aws_subnet.private-a.id
}

output "private-b-subnet-id" {
  value = aws_subnet.private-b.id
}

output "public-a-subnet-id" {
  value = aws_subnet.public-a.id
}

output "public-b-subnet-id" {
  value = aws_subnet.public-b.id
}

output "vpc-id" {
  value = aws_vpc.main.id
}
