resource "aws_vpc_endpoint" "ec2messages" {
  count               = var.ssm == "true" ? 1 : 0
  private_dns_enabled = true
  security_group_ids  = [aws_security_group.endpoints[0].id]
  service_name        = "com.amazonaws.us-east-1.ec2messages"
  subnet_ids          = [aws_subnet.private-a.id, aws_subnet.private-b.id]
  tags = {
    Name    = "${var.vpc-name}-ec2messages"
  }
  vpc_endpoint_type = "Interface"
  vpc_id            = aws_vpc.main.id
}

resource "aws_vpc_endpoint" "ssmmessages" {
  count               = var.ssm == "true" ? 1 : 0
  private_dns_enabled = true
  security_group_ids  = [aws_security_group.endpoints[0].id]
  service_name        = "com.amazonaws.us-east-1.ssmmessages"
  subnet_ids          = [aws_subnet.private-a.id, aws_subnet.private-b.id]
  tags = {
    Name    = "${var.vpc-name}-ssmmessages"
  }
  vpc_endpoint_type = "Interface"
  vpc_id            = aws_vpc.main.id
}

resource "aws_vpc_endpoint" "ssm" {
  count               = var.ssm == "true" ? 1 : 0
  private_dns_enabled = true
  security_group_ids  = [aws_security_group.endpoints[0].id]
  service_name        = "com.amazonaws.us-east-1.ssm"
  subnet_ids          = [aws_subnet.private-a.id, aws_subnet.private-b.id]
  tags = {
    Name    = "${var.vpc-name}-endpoints-ssm"
  }
  vpc_endpoint_type = "Interface"
  vpc_id            = aws_vpc.main.id
}
