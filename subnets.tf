resource "aws_subnet" "public-a" {
  availability_zone       = "us-east-1a"
  cidr_block              = "10.0.0.0/24"
  map_public_ip_on_launch = "true"
  tags = {
    Name = "${var.vpc-name}-public-a"
  }
  vpc_id = aws_vpc.main.id
}

resource "aws_subnet" "public-b" {
  availability_zone       = "us-east-1b"
  cidr_block              = "10.0.1.0/24"
  map_public_ip_on_launch = "true"
  tags = {
    Name = "${var.vpc-name}-public-b"
  }
  vpc_id = aws_vpc.main.id
}

resource "aws_subnet" "private-a" {
  availability_zone       = "us-east-1a"
  cidr_block              = "10.0.2.0/24"
  map_public_ip_on_launch = "false"
  tags = {
    Name = "${var.vpc-name}-private-a"
  }
  vpc_id = aws_vpc.main.id
}

resource "aws_subnet" "private-b" {
  availability_zone       = "us-east-1b"
  cidr_block              = "10.0.3.0/24"
  map_public_ip_on_launch = "false"
  tags = {
    Name = "${var.vpc-name}-private-b"
  }
  vpc_id = aws_vpc.main.id
}
